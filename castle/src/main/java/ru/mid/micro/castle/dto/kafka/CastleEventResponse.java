package ru.mid.micro.castle.dto.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.mid.micro.castle.dto.BuildingStatus;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
@EqualsAndHashCode
@ToString
public class CastleEventResponse {

    private UUID id;

    private Integer heightInMeters;

    private BuildingStatus status;

}
