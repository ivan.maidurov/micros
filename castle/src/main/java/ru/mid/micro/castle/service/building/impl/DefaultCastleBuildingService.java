package ru.mid.micro.castle.service.building.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.mid.micro.castle.service.model.Castle;
import ru.mid.micro.castle.service.building.CastleBuildingService;

import java.time.Duration;

@Service
@Slf4j
public class DefaultCastleBuildingService implements CastleBuildingService {

    @Override
    public void build(Castle castle) {
        Mono.just(castle)
                .delayElement(Duration.ofSeconds(2))
                .doOnError(e -> log.error("Failed to build: ", e))
                .doOnSuccess(r -> log.info("Successfully built: {}", r))
                .subscribe();
    }

}
