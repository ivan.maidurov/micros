package ru.mid.micro.castle.service.kafka.consumer;

import reactor.core.publisher.Flux;
import ru.mid.micro.castle.dto.kafka.CastleCommandDto;

public interface KafkaConsumerService {

    Flux<CastleCommandDto> consumeEvent();

}
