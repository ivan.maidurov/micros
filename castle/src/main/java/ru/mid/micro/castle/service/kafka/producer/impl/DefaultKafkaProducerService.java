package ru.mid.micro.castle.service.kafka.producer.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;
import ru.mid.micro.castle.dto.kafka.CastleEventResponse;
import ru.mid.micro.castle.service.kafka.producer.KafkaProducerService;

@Service
public class DefaultKafkaProducerService implements KafkaProducerService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultKafkaProducerService.class);

    private final ReactiveKafkaProducerTemplate<String, CastleEventResponse> reactiveKafkaProducerTemplate;

    private final String topic;

    public DefaultKafkaProducerService(@Value("${app.kafka.event-topic}") String topic,
                                       ReactiveKafkaProducerTemplate<String, CastleEventResponse> reactiveKafkaProducerTemplate) {
        this.topic = topic;
        this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
    }

    public void send(CastleEventResponse castleEventResponse) {
        LOG.info("send to topic={}, {}={},", topic, CastleEventResponse.class.getSimpleName(), castleEventResponse);
        reactiveKafkaProducerTemplate.send(topic, castleEventResponse)
                .doOnSuccess(senderResult -> LOG.info("sent {} offset : {}", castleEventResponse, senderResult.recordMetadata().offset()))
                .subscribe();
    }
}
