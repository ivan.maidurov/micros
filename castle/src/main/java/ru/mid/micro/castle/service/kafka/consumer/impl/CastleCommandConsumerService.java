package ru.mid.micro.castle.service.kafka.consumer.impl;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import ru.mid.micro.castle.dto.kafka.CastleCommandDto;
import ru.mid.micro.castle.service.building.CastleBuildingService;
import ru.mid.micro.castle.service.kafka.consumer.KafkaConsumerService;

import java.time.Duration;

@Service
public class CastleCommandConsumerService implements KafkaConsumerService {
    private static final Logger LOG = LoggerFactory.getLogger(CastleCommandConsumerService.class);

    private final ReactiveKafkaConsumerTemplate<String, CastleCommandDto> castleCommandConsumerTemplate;
    private final CastleBuildingService castleBuildingService;

    public CastleCommandConsumerService(ReactiveKafkaConsumerTemplate<String, CastleCommandDto> castleCommandConsumerTemplate,
                                        CastleBuildingService castleBuildingService) {
        this.castleCommandConsumerTemplate = castleCommandConsumerTemplate;
        this.castleBuildingService = castleBuildingService;
    }

    public Flux<CastleCommandDto> consumeEvent() {
        return castleCommandConsumerTemplate
                .receiveAutoAck()
                .delayElements(Duration.ofSeconds(2L)) // BACKPRESSURE
                .doOnNext(consumerRecord -> LOG.info("received key={}, value={} from topic={}, offset={}",
                        consumerRecord.key(),
                        consumerRecord.value(),
                        consumerRecord.topic(),
                        consumerRecord.offset())
                )
                .map(ConsumerRecord::value)
                .doOnError(throwable -> LOG.error("something bad happened while consuming : {}", throwable.getMessage()))
                .onErrorContinue((e, f) -> {
                    LOG.error("shiii... {}", f, e);
                });
    }
}
