package ru.mid.micro.castle.dto;

public enum BuildingStatus {

    BUILDING,
    BUILT;

}
