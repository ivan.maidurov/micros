package ru.mid.micro.castle.service.part.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.mid.micro.castle.service.part.ParticipantService;
import ru.mid.micro.castle.dto.BuildingStatus;
import ru.mid.micro.castle.dto.kafka.CastleCommandDto;
import ru.mid.micro.castle.dto.kafka.CastleEventResponse;
import ru.mid.micro.castle.service.building.CastleBuildingService;
import ru.mid.micro.castle.service.kafka.consumer.KafkaConsumerService;
import ru.mid.micro.castle.service.kafka.producer.KafkaProducerService;
import ru.mid.micro.castle.service.model.Castle;

import java.util.UUID;

@Service
@Slf4j
public class DefaultParticipantService implements ParticipantService {

    private final CastleBuildingService castleBuildingService;
    private final KafkaConsumerService consumerService;
    private final KafkaProducerService producerService;

    public DefaultParticipantService(CastleBuildingService castleBuildingService,
                                     KafkaConsumerService consumerService,
                                     KafkaProducerService producerService) {
        this.castleBuildingService = castleBuildingService;
        this.consumerService = consumerService;
        this.producerService = producerService;
    }

    private Flux<Castle> doSmth() {
        return consumerService.consumeEvent()
                .map(castleCommand -> {
                    UUID newTowerId = UUID.randomUUID();
                    return Castle.builder()
                            .id(newTowerId)
                            .heightInMeters(castleCommand.getHeightInMeters())
                            .build();
                })
                .doOnNext(castle -> {
                    Mono.just(CastleEventResponse.builder()
                                    .id(castle.getId())
                                    .heightInMeters(castle.getHeightInMeters())
                                    .status(BuildingStatus.BUILDING)
                                    .build())
                            .doOnNext(producerService::send)
                            .subscribe();
                })
                .doOnNext(castleBuildingService::build)
                .doOnNext(castle -> {
                            Mono.just(CastleEventResponse.builder()
                                            .id(castle.getId())
                                            .heightInMeters(castle.getHeightInMeters())
                                            .status(BuildingStatus.BUILT)
                                            .build())
                                    .doOnNext(producerService::send)
                                    .subscribe();
                        })
                .doOnNext(kafkaRequest -> log.info("successfully consumed {}={}", CastleCommandDto.class.getSimpleName(), kafkaRequest));
    }

    @Override
    public void run() {
        doSmth().subscribe();
    }
}
