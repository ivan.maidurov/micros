package ru.mid.micro.castle.service.building;

import ru.mid.micro.castle.service.model.Castle;

public interface CastleBuildingService {

    void build(Castle castle);

}
