package ru.mid.micro.castle.service.kafka.producer;

import ru.mid.micro.castle.dto.kafka.CastleEventResponse;

public interface KafkaProducerService {

    void send(CastleEventResponse castleEventResponse);

}
