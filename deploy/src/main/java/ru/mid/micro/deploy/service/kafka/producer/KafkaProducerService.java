package ru.mid.micro.deploy.service.kafka.producer;

import ru.mid.micro.deploy.dto.kafka.DeployEventResponse;

public interface KafkaProducerService {

    void send(DeployEventResponse deployEventResponse);

}
