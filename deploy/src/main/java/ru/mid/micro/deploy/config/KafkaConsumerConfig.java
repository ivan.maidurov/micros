package ru.mid.micro.deploy.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import reactor.kafka.receiver.ReceiverOptions;
import ru.mid.micro.deploy.dto.kafka.DeployCommandDto;

import java.util.Collections;
import java.util.Map;

@Configuration
public class KafkaConsumerConfig {

    private final String deployTopic;

    public KafkaConsumerConfig(@Value("${app.kafka.command-topic}") String deployTopic) {
        this.deployTopic = deployTopic;
    }

    @Bean
    public ReactiveKafkaConsumerTemplate<String, DeployCommandDto> wallCommandConsumerTemplate(KafkaProperties kafkaProperties) {
        ReceiverOptions<String, DeployCommandDto> kafkaReceiverOptions = configureKafkaForTopic(kafkaProperties, deployTopic, DeployCommandDto.class);
        return new ReactiveKafkaConsumerTemplate<>(kafkaReceiverOptions);
    }

    private <T> ReceiverOptions<String, T> configureKafkaForTopic(KafkaProperties kafkaProperties, String topic, Class<T> defaultType) {
        Map<String, Object> defaultConsumerProperties = kafkaProperties.buildConsumerProperties();

        defaultConsumerProperties.put(JsonDeserializer.VALUE_DEFAULT_TYPE, defaultType);

        return ReceiverOptions
                .<String, T>create(defaultConsumerProperties)
                .subscription(Collections.singletonList(topic));
    }

}
