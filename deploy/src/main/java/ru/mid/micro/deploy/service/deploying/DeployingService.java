package ru.mid.micro.deploy.service.deploying;

import ru.mid.micro.deploy.service.model.Deploy;

public interface DeployingService {

    void build(Deploy deploy);

}
