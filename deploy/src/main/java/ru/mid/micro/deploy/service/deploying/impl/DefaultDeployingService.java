package ru.mid.micro.deploy.service.deploying.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.mid.micro.deploy.service.deploying.DeployingService;
import ru.mid.micro.deploy.service.model.Deploy;

import java.time.Duration;

@Service
@Slf4j
public class DefaultDeployingService implements DeployingService {

    @Override
    public void build(Deploy deploy) {
        Mono.just(deploy)
                .delayElement(Duration.ofSeconds(2))
                .doOnError(e -> log.error("Failed to build: ", e))
                .doOnSuccess(r -> log.info("Successfully built: {}", r))
                .subscribe();
    }

}
