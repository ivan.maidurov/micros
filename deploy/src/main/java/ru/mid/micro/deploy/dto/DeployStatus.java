package ru.mid.micro.deploy.dto;

public enum DeployStatus {

    DEPLOYING,
    DEPLOY_ERROR,
    DEPLOYED

}
