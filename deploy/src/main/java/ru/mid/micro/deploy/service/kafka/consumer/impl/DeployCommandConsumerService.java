package ru.mid.micro.deploy.service.kafka.consumer.impl;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import ru.mid.micro.deploy.dto.kafka.DeployCommandDto;
import ru.mid.micro.deploy.service.deploying.DeployingService;
import ru.mid.micro.deploy.service.kafka.consumer.KafkaConsumerService;

import java.time.Duration;

@Service
public class DeployCommandConsumerService implements KafkaConsumerService {
    private static final Logger LOG = LoggerFactory.getLogger(DeployCommandConsumerService.class);

    private final ReactiveKafkaConsumerTemplate<String, DeployCommandDto> wallCommandConsumerTemplate;
    private final DeployingService deployingService;

    public DeployCommandConsumerService(ReactiveKafkaConsumerTemplate<String, DeployCommandDto> wallCommandConsumerTemplate,
                                        DeployingService deployingService) {
        this.wallCommandConsumerTemplate = wallCommandConsumerTemplate;
        this.deployingService = deployingService;
    }

    public Flux<DeployCommandDto> consumeEvent() {
        return wallCommandConsumerTemplate
                .receiveAutoAck()
                .delayElements(Duration.ofSeconds(2L)) // BACKPRESSURE
                .doOnNext(consumerRecord -> LOG.info("received key={}, value={} from topic={}, offset={}",
                        consumerRecord.key(),
                        consumerRecord.value(),
                        consumerRecord.topic(),
                        consumerRecord.offset())
                )
                .map(ConsumerRecord::value)
                .doOnError(throwable -> LOG.error("something bad happened while consuming : {}", throwable.getMessage()))
                .onErrorContinue((e, f) -> {
                    LOG.error("shiii... {}", f, e);
                });
    }
}
