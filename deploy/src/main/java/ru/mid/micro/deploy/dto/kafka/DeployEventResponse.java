package ru.mid.micro.deploy.dto.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.mid.micro.deploy.dto.DeployStatus;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
@EqualsAndHashCode
@ToString
public class DeployEventResponse {

    private UUID id;

    private Integer heightInMeters;

    private Integer lengthInMeters;

    private DeployStatus status;

}
