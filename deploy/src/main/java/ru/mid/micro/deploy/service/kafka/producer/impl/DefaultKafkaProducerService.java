package ru.mid.micro.deploy.service.kafka.producer.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;
import ru.mid.micro.deploy.dto.kafka.DeployEventResponse;
import ru.mid.micro.deploy.service.kafka.producer.KafkaProducerService;

@Service
public class DefaultKafkaProducerService implements KafkaProducerService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultKafkaProducerService.class);

    private final ReactiveKafkaProducerTemplate<String, DeployEventResponse> reactiveKafkaProducerTemplate;

    private final String topic;

    public DefaultKafkaProducerService(@Value("${app.kafka.event-topic}") String topic,
                                       ReactiveKafkaProducerTemplate<String, DeployEventResponse> reactiveKafkaProducerTemplate) {
        this.topic = topic;
        this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
    }

    public void send(DeployEventResponse deployEventResponse) {
        LOG.info("send to topic={}, {}={},", topic, DeployEventResponse.class.getSimpleName(), deployEventResponse);
        reactiveKafkaProducerTemplate.send(topic, deployEventResponse)
                .doOnSuccess(senderResult -> LOG.info("sent {} offset : {}", deployEventResponse, senderResult.recordMetadata().offset()))
                .subscribe();
    }
}
