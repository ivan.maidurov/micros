package ru.mid.micro.deploy.service.kafka.consumer;

import reactor.core.publisher.Flux;
import ru.mid.micro.deploy.dto.kafka.DeployCommandDto;

public interface KafkaConsumerService {

    Flux<DeployCommandDto> consumeEvent();

}
