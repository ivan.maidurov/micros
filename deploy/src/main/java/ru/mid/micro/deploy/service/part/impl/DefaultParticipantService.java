package ru.mid.micro.deploy.service.part.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.mid.micro.deploy.dto.DeployStatus;
import ru.mid.micro.deploy.dto.kafka.DeployCommandDto;
import ru.mid.micro.deploy.dto.kafka.DeployEventResponse;
import ru.mid.micro.deploy.service.deploying.DeployingService;
import ru.mid.micro.deploy.service.kafka.consumer.KafkaConsumerService;
import ru.mid.micro.deploy.service.kafka.producer.KafkaProducerService;
import ru.mid.micro.deploy.service.model.Deploy;
import ru.mid.micro.deploy.service.part.ParticipantService;

import java.util.UUID;

@Service
@Slf4j
public class DefaultParticipantService implements ParticipantService {

    private final DeployingService deployingService;
    private final KafkaConsumerService consumerService;
    private final KafkaProducerService producerService;

    public DefaultParticipantService(DeployingService deployingService,
                                     KafkaConsumerService consumerService,
                                     KafkaProducerService producerService) {
        this.deployingService = deployingService;
        this.consumerService = consumerService;
        this.producerService = producerService;
    }

    private Flux<Deploy> doSmth() {
        return consumerService.consumeEvent()
                .map(deployCommand -> {
                    UUID newTowerId = UUID.randomUUID();
                    return Deploy.builder()
                            .id(newTowerId)
                            .heightInMeters(deployCommand.getHeightInMeters())
                            .lengthInMeters(deployCommand.getLengthInMeters())
                            .build();
                })
                .doOnNext(deploy -> {
                    Mono.just(DeployEventResponse.builder()
                                    .id(deploy.getId())
                                    .heightInMeters(deploy.getHeightInMeters())
                                    .lengthInMeters(deploy.getLengthInMeters())
                                    .status(DeployStatus.DEPLOYING)
                                    .build())
                            .doOnNext(producerService::send)
                            .subscribe();
                })
                .doOnNext(deployingService::build)
                .doOnNext(deploy -> {
                            Mono.just(DeployEventResponse.builder()
                                            .id(deploy.getId())
                                            .heightInMeters(deploy.getHeightInMeters())
                                            .lengthInMeters(deploy.getLengthInMeters())
                                            .status(DeployStatus.DEPLOYED)
                                            .build())
                                    .doOnNext(producerService::send)
                                    .subscribe();
                        })
                .doOnNext(kafkaRequest -> log.info("successfully consumed {}={}", DeployCommandDto.class.getSimpleName(), kafkaRequest));
    }

    @Override
    public void run() {
        doSmth().subscribe();
    }
}
