package ru.mid.micro.tower.service.kafka.producer.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;
import ru.mid.micro.tower.dto.kafka.TowerEventResponse;
import ru.mid.micro.tower.service.kafka.producer.KafkaProducerService;

@Service
public class DefaultKafkaProducerService implements KafkaProducerService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultKafkaProducerService.class);

    private final ReactiveKafkaProducerTemplate<String, TowerEventResponse> reactiveKafkaProducerTemplate;

    private final String topic;

    public DefaultKafkaProducerService(@Value("${app.kafka.event-topic}") String topic,
                                       ReactiveKafkaProducerTemplate<String, TowerEventResponse> reactiveKafkaProducerTemplate) {
        this.topic = topic;
        this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
    }

    public void send(TowerEventResponse towerEventResponse) {
        LOG.info("send to topic={}, {}={},", topic, TowerEventResponse.class.getSimpleName(), towerEventResponse);
        reactiveKafkaProducerTemplate.send(topic, towerEventResponse)
                .doOnSuccess(senderResult -> LOG.info("sent {} offset : {}", towerEventResponse, senderResult.recordMetadata().offset()))
                .subscribe();
    }
}
