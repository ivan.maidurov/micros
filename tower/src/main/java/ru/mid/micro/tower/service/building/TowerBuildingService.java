package ru.mid.micro.tower.service.building;

import ru.mid.micro.tower.service.model.Tower;

public interface TowerBuildingService {

    void build(Tower tower);

}
