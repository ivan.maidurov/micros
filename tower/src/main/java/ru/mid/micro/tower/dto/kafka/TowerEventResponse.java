package ru.mid.micro.tower.dto.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.mid.micro.tower.dto.BuildingStatus;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
@EqualsAndHashCode
@ToString
public class TowerEventResponse {

    private UUID id;

    private Integer heightInMeters;

    private BuildingStatus status;

}
