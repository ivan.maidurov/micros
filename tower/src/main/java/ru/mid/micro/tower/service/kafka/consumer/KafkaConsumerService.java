package ru.mid.micro.tower.service.kafka.consumer;

import reactor.core.publisher.Flux;
import ru.mid.micro.tower.dto.kafka.TowerCommandDto;

public interface KafkaConsumerService {

    Flux<TowerCommandDto> consumeEvent();

}
