package ru.mid.micro.tower.service.kafka.producer;

import ru.mid.micro.tower.dto.kafka.TowerEventResponse;

public interface KafkaProducerService {

    void send(TowerEventResponse towerEventResponse);

}
