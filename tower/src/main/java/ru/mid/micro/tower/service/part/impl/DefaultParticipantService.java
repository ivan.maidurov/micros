package ru.mid.micro.tower.service.part.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.mid.micro.tower.dto.BuildingStatus;
import ru.mid.micro.tower.dto.kafka.TowerCommandDto;
import ru.mid.micro.tower.dto.kafka.TowerEventResponse;
import ru.mid.micro.tower.service.building.TowerBuildingService;
import ru.mid.micro.tower.service.kafka.consumer.KafkaConsumerService;
import ru.mid.micro.tower.service.kafka.producer.KafkaProducerService;
import ru.mid.micro.tower.service.model.Tower;
import ru.mid.micro.tower.service.part.ParticipantService;

import java.util.UUID;

@Service
@Slf4j
public class DefaultParticipantService implements ParticipantService {

    private final TowerBuildingService towerBuildingService;
    private final KafkaConsumerService consumerService;
    private final KafkaProducerService producerService;

    public DefaultParticipantService(TowerBuildingService towerBuildingService,
                                     KafkaConsumerService consumerService,
                                     KafkaProducerService producerService) {
        this.towerBuildingService = towerBuildingService;
        this.consumerService = consumerService;
        this.producerService = producerService;
    }

    private Flux<Tower> doSmth() {
        return consumerService.consumeEvent()
                .map(towerCommand -> {
                    UUID newTowerId = UUID.randomUUID();
                    return Tower.builder()
                            .id(newTowerId)
                            .heightInMeters(towerCommand.getHeightInMeters())
                            .build();
                })
                .doOnNext(tower -> {
                    Mono.just(TowerEventResponse.builder()
                                    .id(tower.getId())
                                    .heightInMeters(tower.getHeightInMeters())
                                    .status(BuildingStatus.BUILDING)
                                    .build())
                            .doOnNext(producerService::send)
                            .subscribe();
                })
                .doOnNext(towerBuildingService::build)
                .doOnNext(tower -> {
                            Mono.just(TowerEventResponse.builder()
                                            .id(tower.getId())
                                            .heightInMeters(tower.getHeightInMeters())
                                            .status(BuildingStatus.BUILT)
                                            .build())
                                    .doOnNext(producerService::send)
                                    .subscribe();
                        })
                .doOnNext(kafkaRequest -> log.info("successfully consumed {}={}", TowerCommandDto.class.getSimpleName(), kafkaRequest));
    }

    @Override
    public void run() {
        doSmth().subscribe();
    }
}
