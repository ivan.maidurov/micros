package ru.mid.micro.tower.service.building.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.mid.micro.tower.service.building.TowerBuildingService;
import ru.mid.micro.tower.service.model.Tower;

import java.time.Duration;

@Service
@Slf4j
public class DefaultTowerBuildingService implements TowerBuildingService {

    @Override
    public void build(Tower tower) {
        Mono.just(tower)
                .delayElement(Duration.ofSeconds(2))
                .doOnError(e -> log.error("Failed to build: ", e))
                .doOnSuccess(r -> log.info("Successfully built: {}", r))
                .subscribe();
    }

}
