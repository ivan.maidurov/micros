package ru.mid.micro.tower.dto;

public enum BuildingStatus {

    BUILDING,
    BUILT;

}
