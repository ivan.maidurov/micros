package ru.mid.micro.tower.service.kafka.consumer.impl;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import ru.mid.micro.tower.dto.kafka.TowerCommandDto;
import ru.mid.micro.tower.service.building.TowerBuildingService;
import ru.mid.micro.tower.service.kafka.consumer.KafkaConsumerService;

import java.time.Duration;

@Service
public class TowerCommandConsumerService implements KafkaConsumerService {
    private static final Logger LOG = LoggerFactory.getLogger(TowerCommandConsumerService.class);

    private final ReactiveKafkaConsumerTemplate<String, TowerCommandDto> towerCommandConsumerTemplate;
    private final TowerBuildingService towerBuildingService;

    public TowerCommandConsumerService(ReactiveKafkaConsumerTemplate<String, TowerCommandDto> towerCommandConsumerTemplate,
                                       TowerBuildingService towerBuildingService) {
        this.towerCommandConsumerTemplate = towerCommandConsumerTemplate;
        this.towerBuildingService = towerBuildingService;
    }

    public Flux<TowerCommandDto> consumeEvent() {
        return towerCommandConsumerTemplate
                .receiveAutoAck()
                .delayElements(Duration.ofSeconds(2L)) // BACKPRESSURE
                .doOnNext(consumerRecord -> LOG.info("received key={}, value={} from topic={}, offset={}",
                        consumerRecord.key(),
                        consumerRecord.value(),
                        consumerRecord.topic(),
                        consumerRecord.offset())
                )
                .map(ConsumerRecord::value)
                .doOnError(throwable -> LOG.error("something bad happened while consuming : {}", throwable.getMessage()))
                .onErrorContinue((e, f) -> {
                    LOG.error("shiii... {}", f, e);
                });
    }
}
