package ru.mid.micro.pack.service.kafka.consumer.impl;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import ru.mid.micro.deploy.service.kafka.consumer.KafkaConsumerService;
import ru.mid.micro.pack.dto.kafka.PackCommandDto;
import ru.mid.micro.pack.service.deploying.PackingService;

import java.time.Duration;

@Service
public class PackCommandConsumerService implements KafkaConsumerService {
    private static final Logger LOG = LoggerFactory.getLogger(PackCommandConsumerService.class);

    private final ReactiveKafkaConsumerTemplate<String, PackCommandDto> packCommandConsumerTemplate;
    private final PackingService packingService;

    public PackCommandConsumerService(ReactiveKafkaConsumerTemplate<String, PackCommandDto> packCommandConsumerTemplate,
                                      PackingService packingService) {
        this.packCommandConsumerTemplate = packCommandConsumerTemplate;
        this.packingService = packingService;
    }

    public Flux<PackCommandDto> consumeEvent() {
        return packCommandConsumerTemplate
                .receiveAutoAck()
                .delayElements(Duration.ofSeconds(2L)) // BACKPRESSURE
                .doOnNext(consumerRecord -> LOG.info("received key={}, value={} from topic={}, offset={}",
                        consumerRecord.key(),
                        consumerRecord.value(),
                        consumerRecord.topic(),
                        consumerRecord.offset())
                )
                .map(ConsumerRecord::value)
                .doOnError(throwable -> LOG.error("something bad happened while consuming : {}", throwable.getMessage()))
                .onErrorContinue((e, f) -> {
                    LOG.error("shiii... {}", f, e);
                });
    }
}
