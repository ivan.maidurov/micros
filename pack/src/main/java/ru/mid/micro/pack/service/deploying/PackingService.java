package ru.mid.micro.pack.service.deploying;

import ru.mid.micro.pack.service.model.Pack;

public interface PackingService {

    void build(Pack pack);

}
