package ru.mid.micro.pack.scheduler;

import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.mid.micro.deploy.service.kafka.consumer.KafkaConsumerService;
import ru.mid.micro.pack.dto.kafka.PackEventResponse;
import ru.mid.micro.pack.service.kafka.producer.KafkaProducerService;

import java.util.Set;

@Component
public class StartupListener implements ApplicationListener<ApplicationStartedEvent> {

    private final Set<KafkaProducerService> kafkaProducerServices;
    private final Set<KafkaConsumerService> kafkaConsumerServices;

    public StartupListener(Set<KafkaProducerService> kafkaProducerServices, Set<KafkaConsumerService> kafkaConsumerServices) {
        this.kafkaProducerServices = kafkaProducerServices;
        this.kafkaConsumerServices = kafkaConsumerServices;
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        kafkaConsumerServices.forEach(Runnable::run);
        kafkaProducerServices.forEach(kafkaProducerService -> kafkaProducerService.send(new PackEventResponse("suk", "tower_name")));
    }
}
