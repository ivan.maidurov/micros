package ru.mid.micro.pack.service.part.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.mid.micro.deploy.service.kafka.consumer.KafkaConsumerService;
import ru.mid.micro.pack.dto.PackStatus;
import ru.mid.micro.pack.dto.kafka.PackCommandDto;
import ru.mid.micro.pack.dto.kafka.PackEventResponse;
import ru.mid.micro.pack.service.deploying.PackingService;
import ru.mid.micro.pack.service.kafka.producer.KafkaProducerService;
import ru.mid.micro.pack.service.model.Pack;
import ru.mid.micro.pack.service.part.ParticipantService;

import java.util.UUID;

@Service
@Slf4j
public class DefaultParticipantService implements ParticipantService {

    private final PackingService packingService;
    private final KafkaConsumerService consumerService;
    private final KafkaProducerService producerService;

    public DefaultParticipantService(PackingService packingService,
                                     KafkaConsumerService consumerService,
                                     KafkaProducerService producerService) {
        this.packingService = packingService;
        this.consumerService = consumerService;
        this.producerService = producerService;
    }

    private Flux<Pack> doSmth() {
        return consumerService.consumeEvent()
                .map(deployCommand -> {
                    UUID newTowerId = UUID.randomUUID();
                    return Pack.builder()
                            .id(newTowerId)
                            .heightInMeters(deployCommand.getHeightInMeters())
                            .lengthInMeters(deployCommand.getLengthInMeters())
                            .build();
                })
                .doOnNext(pack -> {
                    Mono.just(PackEventResponse.builder()
                                    .id(pack.getId())
                                    .heightInMeters(pack.getHeightInMeters())
                                    .lengthInMeters(pack.getLengthInMeters())
                                    .status(PackStatus.PACKING)
                                    .build())
                            .doOnNext(producerService::send)
                            .subscribe();
                })
                .doOnNext(packingService::build)
                .doOnNext(pack -> {
                            Mono.just(PackEventResponse.builder()
                                            .id(pack.getId())
                                            .heightInMeters(pack.getHeightInMeters())
                                            .lengthInMeters(pack.getLengthInMeters())
                                            .status(PackStatus.PACKED)
                                            .build())
                                    .doOnNext(producerService::send)
                                    .subscribe();
                        })
                .doOnNext(kafkaRequest -> log.info("successfully consumed {}={}", PackCommandDto.class.getSimpleName(), kafkaRequest));
    }

    @Override
    public void run() {
        doSmth().subscribe();
    }
}
