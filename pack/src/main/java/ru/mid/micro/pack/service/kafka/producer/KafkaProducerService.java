package ru.mid.micro.pack.service.kafka.producer;

import ru.mid.micro.pack.dto.kafka.PackEventResponse;

public interface KafkaProducerService {

    void send(PackEventResponse packEventResponse);

}
