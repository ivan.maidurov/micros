package ru.mid.micro.pack.service.deploying.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.mid.micro.pack.service.deploying.PackingService;
import ru.mid.micro.pack.service.model.Pack;

import java.time.Duration;

@Service
@Slf4j
public class DefaultPackingService implements PackingService {

    @Override
    public void build(Pack pack) {
        Mono.just(pack)
                .delayElement(Duration.ofSeconds(2))
                .doOnError(e -> log.error("Failed to build: ", e))
                .doOnSuccess(r -> log.info("Successfully built: {}", r))
                .subscribe();
    }

}
