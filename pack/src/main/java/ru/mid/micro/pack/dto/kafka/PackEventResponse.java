package ru.mid.micro.pack.dto.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.mid.micro.pack.dto.PackStatus;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
@EqualsAndHashCode
@ToString
public class PackEventResponse {

    private UUID id;

    private Integer heightInMeters;

    private Integer lengthInMeters;

    private PackStatus status;

}
