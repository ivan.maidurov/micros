package ru.mid.micro.pack.service.kafka.producer.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;
import ru.mid.micro.pack.dto.kafka.PackEventResponse;
import ru.mid.micro.pack.service.kafka.producer.KafkaProducerService;

@Service
public class DefaultKafkaProducerService implements KafkaProducerService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultKafkaProducerService.class);

    private final ReactiveKafkaProducerTemplate<String, PackEventResponse> reactiveKafkaProducerTemplate;

    private final String topic;

    public DefaultKafkaProducerService(@Value("${app.kafka.event-topic}") String topic,
                                       ReactiveKafkaProducerTemplate<String, PackEventResponse> reactiveKafkaProducerTemplate) {
        this.topic = topic;
        this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
    }

    public void send(PackEventResponse packEventResponse) {
        LOG.info("send to topic={}, {}={},", topic, PackEventResponse.class.getSimpleName(), packEventResponse);
        reactiveKafkaProducerTemplate.send(topic, packEventResponse)
                .doOnSuccess(senderResult -> LOG.info("sent {} offset : {}", packEventResponse, senderResult.recordMetadata().offset()))
                .subscribe();
    }
}
