package ru.mid.micro.deploy.service.kafka.consumer;

import reactor.core.publisher.Flux;
import ru.mid.micro.pack.dto.kafka.PackCommandDto;

public interface KafkaConsumerService {

    Flux<PackCommandDto> consumeEvent();

}
