package ru.mid.micro;

import com.netflix.graphql.dgs.client.GraphQLResponse;
import com.netflix.graphql.dgs.client.MonoGraphQLClient;
import com.netflix.graphql.dgs.client.WebClientGraphQLClient;
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import ru.mid.micro.generated.client.BigTowerGraphQLQuery;
import ru.mid.micro.generated.client.BigTowerProjectionRoot;
import ru.mid.micro.generated.types.BigTower;

@SpringBootApplication
public class MicroApplication {

    public static void main(String[] args) {
        GraphQLQueryRequest request = new GraphQLQueryRequest(
                new BigTowerGraphQLQuery.Builder()
                        .name("big_tower_name")
                        .build(),
                new BigTowerProjectionRoot()
                        .id()
                        .name()
        );

        System.err.println(request.serialize());

        WebClient webClient = WebClient.create("http://localhost:8080/graphql");

        WebClientGraphQLClient client = MonoGraphQLClient.createWithWebClient(webClient);

        Mono<GraphQLResponse> graphQLResponseMono = client.reactiveExecuteQuery(request.serialize());

        BigTower bigTower = graphQLResponseMono
                .doOnError(System.err::println)
                .doOnSuccess(System.err::println)
                .map(r -> r.extractValueAsObject("bigTower", BigTower.class))
                .block();

        System.err.println(bigTower);
    }

}
