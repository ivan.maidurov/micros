package ru.mid.micro.wall.dto.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.mid.micro.wall.dto.BuildingStatus;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
@EqualsAndHashCode
@ToString
public class WallEventResponse {

    private UUID id;

    private Integer heightInMeters;

    private Integer lengthInMeters;

    private BuildingStatus status;

}
