package ru.mid.micro.wall.service.kafka.producer;

import ru.mid.micro.wall.dto.kafka.WallEventResponse;

public interface KafkaProducerService {

    void send(WallEventResponse wallEventResponse);

}
