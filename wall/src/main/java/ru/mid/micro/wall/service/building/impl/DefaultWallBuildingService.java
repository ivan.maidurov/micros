package ru.mid.micro.wall.service.building.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.mid.micro.wall.service.building.WallBuildingService;
import ru.mid.micro.wall.service.model.Wall;

import java.time.Duration;

@Service
@Slf4j
public class DefaultWallBuildingService implements WallBuildingService {

    @Override
    public void build(Wall wall) {
        Mono.just(wall)
                .delayElement(Duration.ofSeconds(2))
                .doOnError(e -> log.error("Failed to build: ", e))
                .doOnSuccess(r -> log.info("Successfully built: {}", r))
                .subscribe();
    }

}
