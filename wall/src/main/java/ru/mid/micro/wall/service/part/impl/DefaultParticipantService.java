package ru.mid.micro.wall.service.part.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.mid.micro.wall.dto.BuildingStatus;
import ru.mid.micro.wall.dto.kafka.WallCommandDto;
import ru.mid.micro.wall.dto.kafka.WallEventResponse;
import ru.mid.micro.wall.service.building.WallBuildingService;
import ru.mid.micro.wall.service.kafka.consumer.KafkaConsumerService;
import ru.mid.micro.wall.service.kafka.producer.KafkaProducerService;
import ru.mid.micro.wall.service.model.Wall;
import ru.mid.micro.wall.service.part.ParticipantService;

import java.util.UUID;

@Service
@Slf4j
public class DefaultParticipantService implements ParticipantService {

    private final WallBuildingService wallBuildingService;
    private final KafkaConsumerService consumerService;
    private final KafkaProducerService producerService;

    public DefaultParticipantService(WallBuildingService wallBuildingService,
                                     KafkaConsumerService consumerService,
                                     KafkaProducerService producerService) {
        this.wallBuildingService = wallBuildingService;
        this.consumerService = consumerService;
        this.producerService = producerService;
    }

    private Flux<Wall> doSmth() {
        return consumerService.consumeEvent()
                .map(wallCommand -> {
                    UUID newTowerId = UUID.randomUUID();
                    return Wall.builder()
                            .id(newTowerId)
                            .heightInMeters(wallCommand.getHeightInMeters())
                            .lengthInMeters(wallCommand.getLengthInMeters())
                            .build();
                })
                .doOnNext(wall -> {
                    Mono.just(WallEventResponse.builder()
                                    .id(wall.getId())
                                    .heightInMeters(wall.getHeightInMeters())
                                    .lengthInMeters(wall.getLengthInMeters())
                                    .status(BuildingStatus.BUILDING)
                                    .build())
                            .doOnNext(producerService::send)
                            .subscribe();
                })
                .doOnNext(wallBuildingService::build)
                .doOnNext(wall -> {
                            Mono.just(WallEventResponse.builder()
                                            .id(wall.getId())
                                            .heightInMeters(wall.getHeightInMeters())
                                            .lengthInMeters(wall.getLengthInMeters())
                                            .status(BuildingStatus.BUILT)
                                            .build())
                                    .doOnNext(producerService::send)
                                    .subscribe();
                        })
                .doOnNext(kafkaRequest -> log.info("successfully consumed {}={}", WallCommandDto.class.getSimpleName(), kafkaRequest));
    }

    @Override
    public void run() {
        doSmth().subscribe();
    }
}
