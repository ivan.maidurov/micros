package ru.mid.micro.wall.dto;

public enum BuildingStatus {

    BUILDING,
    BUILT;

}
