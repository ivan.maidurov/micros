package ru.mid.micro.wall.service.building;

import ru.mid.micro.wall.service.model.Wall;

public interface WallBuildingService {

    void build(Wall wall);

}
