package ru.mid.micro.wall.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import reactor.kafka.receiver.ReceiverOptions;
import ru.mid.micro.wall.dto.kafka.WallCommandDto;

import java.util.Collections;
import java.util.Map;

@Configuration
public class KafkaConsumerConfig {

    private final String wallTopic;

    public KafkaConsumerConfig(@Value("${app.kafka.command-topic}") String wallTopic) {
        this.wallTopic = wallTopic;
    }

    @Bean
    public ReactiveKafkaConsumerTemplate<String, WallCommandDto> wallCommandConsumerTemplate(KafkaProperties kafkaProperties) {
        ReceiverOptions<String, WallCommandDto> kafkaReceiverOptions = configureKafkaForTopic(kafkaProperties, wallTopic, WallCommandDto.class);
        return new ReactiveKafkaConsumerTemplate<>(kafkaReceiverOptions);
    }

    private <T> ReceiverOptions<String, T> configureKafkaForTopic(KafkaProperties kafkaProperties, String topic, Class<T> defaultType) {
        Map<String, Object> defaultConsumerProperties = kafkaProperties.buildConsumerProperties();

        defaultConsumerProperties.put(JsonDeserializer.VALUE_DEFAULT_TYPE, defaultType);

        return ReceiverOptions
                .<String, T>create(defaultConsumerProperties)
                .subscription(Collections.singletonList(topic));
    }

}
