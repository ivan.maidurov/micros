package ru.mid.micro.wall.service.kafka.consumer;

import reactor.core.publisher.Flux;
import ru.mid.micro.wall.dto.kafka.WallCommandDto;

public interface KafkaConsumerService {

    Flux<WallCommandDto> consumeEvent();

}
