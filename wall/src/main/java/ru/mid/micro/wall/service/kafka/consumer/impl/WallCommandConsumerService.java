package ru.mid.micro.wall.service.kafka.consumer.impl;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import ru.mid.micro.wall.dto.kafka.WallCommandDto;
import ru.mid.micro.wall.service.building.WallBuildingService;
import ru.mid.micro.wall.service.kafka.consumer.KafkaConsumerService;

import java.time.Duration;

@Service
public class WallCommandConsumerService implements KafkaConsumerService {
    private static final Logger LOG = LoggerFactory.getLogger(WallCommandConsumerService.class);

    private final ReactiveKafkaConsumerTemplate<String, WallCommandDto> wallCommandConsumerTemplate;
    private final WallBuildingService wallBuildingService;

    public WallCommandConsumerService(ReactiveKafkaConsumerTemplate<String, WallCommandDto> wallCommandConsumerTemplate,
                                      WallBuildingService wallBuildingService) {
        this.wallCommandConsumerTemplate = wallCommandConsumerTemplate;
        this.wallBuildingService = wallBuildingService;
    }

    public Flux<WallCommandDto> consumeEvent() {
        return wallCommandConsumerTemplate
                .receiveAutoAck()
                .delayElements(Duration.ofSeconds(2L)) // BACKPRESSURE
                .doOnNext(consumerRecord -> LOG.info("received key={}, value={} from topic={}, offset={}",
                        consumerRecord.key(),
                        consumerRecord.value(),
                        consumerRecord.topic(),
                        consumerRecord.offset())
                )
                .map(ConsumerRecord::value)
                .doOnError(throwable -> LOG.error("something bad happened while consuming : {}", throwable.getMessage()))
                .onErrorContinue((e, f) -> {
                    LOG.error("shiii... {}", f, e);
                });
    }
}
