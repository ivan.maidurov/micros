package ru.mid.micro.wall.service.kafka.producer.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;
import ru.mid.micro.wall.dto.kafka.WallEventResponse;
import ru.mid.micro.wall.service.kafka.producer.KafkaProducerService;

@Service
public class DefaultKafkaProducerService implements KafkaProducerService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultKafkaProducerService.class);

    private final ReactiveKafkaProducerTemplate<String, WallEventResponse> reactiveKafkaProducerTemplate;

    private final String topic;

    public DefaultKafkaProducerService(@Value("${app.kafka.event-topic}") String topic,
                                       ReactiveKafkaProducerTemplate<String, WallEventResponse> reactiveKafkaProducerTemplate) {
        this.topic = topic;
        this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
    }

    public void send(WallEventResponse wallEventResponse) {
        LOG.info("send to topic={}, {}={},", topic, WallEventResponse.class.getSimpleName(), wallEventResponse);
        reactiveKafkaProducerTemplate.send(topic, wallEventResponse)
                .doOnSuccess(senderResult -> LOG.info("sent {} offset : {}", wallEventResponse, senderResult.recordMetadata().offset()))
                .subscribe();
    }
}
