package ru.mid.micro.tower.graphql.tower;

import com.netflix.graphql.dgs.DgsQueryExecutor;
import com.netflix.graphql.dgs.autoconfig.DgsAutoConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.mid.micro.generated.types.Tower;

@SpringBootTest(classes = {DgsAutoConfiguration.class, TowerResolver.class})
class WallResolverTest {

    @Autowired
    private DgsQueryExecutor dgsQueryExecutor;

    @Test
    void getTowers() {
        String expectedName = "expectedName";

        Tower actual = dgsQueryExecutor.executeAndExtractJsonPathAsObject("{ getTower(name: \"expectedName\") { id, name }}", "data.getTower", Tower.class);

        Assertions.assertEquals(expectedName, actual.getName());
    }

}