package ru.mid.micro.builder.graphql.tower;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import com.netflix.graphql.dgs.InputArgument;
import ru.mid.micro.generated.types.BigTower;

@DgsComponent
public class BigTowerResolver {

    @DgsQuery
    public BigTower bigTower(@InputArgument String name) {
        return new BigTower("id".toUpperCase(), name.toUpperCase());
    }
}
