package ru.mid.micro.builder.service.kafka.producer;

import ru.mid.micro.builder.dto.kafka.command.CastleCommandDto;
import ru.mid.micro.builder.dto.kafka.command.DeployCommandDto;
import ru.mid.micro.builder.dto.kafka.command.PackCommandDto;
import ru.mid.micro.builder.dto.kafka.command.TowerCommandDto;
import ru.mid.micro.builder.dto.kafka.command.WallCommandDto;

public interface KafkaProducerService {

    void sendCommandToCastle(CastleCommandDto command);

    void sendCommandToTower(TowerCommandDto command);

    void sendCommandToWall(WallCommandDto command);

    void sendCommandToDeploy(DeployCommandDto command);

    void sendCommandToPack(PackCommandDto command);
}
