package ru.mid.micro.builder.graphql.upload;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.web.multipart.MultipartFile;
import ru.mid.micro.generated.DgsConstants;

import java.io.IOException;

@DgsComponent
public class UploadResolver {

    @DgsData(parentType = DgsConstants.MUTATION.TYPE_NAME, field = "uploadScriptWithMultipartPOST")
    public boolean uploadScript(DataFetchingEnvironment dfe) throws IOException {
        MultipartFile file = dfe.getArgument("input");
        String content = new String(file.getBytes());
        System.err.println(content);
        return !content.isEmpty();
    }
}
