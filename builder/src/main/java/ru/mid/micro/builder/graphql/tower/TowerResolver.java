package ru.mid.micro.builder.graphql.tower;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import com.netflix.graphql.dgs.InputArgument;
import ru.mid.micro.generated.types.Tower;

@DgsComponent
public class TowerResolver {

    @DgsQuery
    public Tower tower(@InputArgument String name) {
        return new Tower("some", name);
    }
}
