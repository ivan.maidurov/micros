package ru.mid.micro.builder.service.kafka.producer.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;
import ru.mid.micro.builder.dto.kafka.command.CastleCommandDto;
import ru.mid.micro.builder.dto.kafka.command.DeployCommandDto;
import ru.mid.micro.builder.dto.kafka.command.PackCommandDto;
import ru.mid.micro.builder.dto.kafka.command.TowerCommandDto;
import ru.mid.micro.builder.dto.kafka.command.WallCommandDto;
import ru.mid.micro.builder.service.kafka.producer.KafkaProducerService;

@Service
public class DefaultKafkaProducerService implements KafkaProducerService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultKafkaProducerService.class);

    private final ReactiveKafkaProducerTemplate<String, Object> commandProducerTemplate;

    private final String towerCommandTopic;
    private final String castleCommandTopic;
    private final String wallCommandTopic;
    private final String deployCommandTopic;
    private final String packCommandTopic;

    public DefaultKafkaProducerService(@Value("${app.kafka.command-topic.tower}") String towerCommandTopic,
                                       @Value("${app.kafka.command-topic.castle}") String castleCommandTopic,
                                       @Value("${app.kafka.command-topic.wall}") String wallCommandTopic,
                                       @Value("${app.kafka.command-topic.deploy}") String deployCommandTopic,
                                       @Value("${app.kafka.command-topic.pack}") String packCommandTopic,
                                       ReactiveKafkaProducerTemplate<String, Object> commandProducerTemplate) {
        this.towerCommandTopic = towerCommandTopic;
        this.castleCommandTopic = castleCommandTopic;
        this.wallCommandTopic = wallCommandTopic;
        this.deployCommandTopic = deployCommandTopic;
        this.packCommandTopic = packCommandTopic;
        this.commandProducerTemplate = commandProducerTemplate;
    }

    @Override
    public void sendCommandToCastle(CastleCommandDto command) {
        send(command, castleCommandTopic);
    }

    @Override
    public void sendCommandToTower(TowerCommandDto command) {
        send(command, towerCommandTopic);
    }

    @Override
    public void sendCommandToWall(WallCommandDto command) {
        send(command, wallCommandTopic);
    }

    @Override
    public void sendCommandToDeploy(DeployCommandDto command) {
        send(command, deployCommandTopic);
    }

    @Override
    public void sendCommandToPack(PackCommandDto command) {
        send(command, packCommandTopic);
    }

    private void send(Object command, String topic) {
        commandProducerTemplate.send(topic, command)
                .doOnSuccess(senderResult -> LOG.info("sent {} offset : {}", command, senderResult.recordMetadata().offset()))
                .doOnError(e -> LOG.error("Failed to send message: ", e))
                .onErrorContinue((e, o) -> LOG.error("Could not send object: {}", o, e))
                .subscribe();
    }
}
