package ru.mid.micro.builder.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import reactor.kafka.receiver.ReceiverOptions;
import ru.mid.micro.builder.dto.kafka.event.CastleEventDto;
import ru.mid.micro.builder.dto.kafka.event.DeployEventDto;
import ru.mid.micro.builder.dto.kafka.event.PackEventDto;
import ru.mid.micro.builder.dto.kafka.event.TowerEventDto;
import ru.mid.micro.builder.dto.kafka.event.WallEventDto;

import java.util.Collections;
import java.util.Map;

@Configuration
public class KafkaConsumerConfig {

    private final String towerTopic;
    private final String castleTopic;
    private final String wallTopic;
    private final String deployTopic;
    private final String packTopic;

    public KafkaConsumerConfig(@Value("${app.kafka.event-topic.tower}") String towerTopic,
                               @Value("${app.kafka.event-topic.castle}") String castleTopic,
                               @Value("${app.kafka.event-topic.wall}") String wallTopic,
                               @Value("${app.kafka.event-topic.deploy}") String deployTopic,
                               @Value("${app.kafka.event-topic.pack}") String packTopic) {
        this.towerTopic = towerTopic;
        this.castleTopic = castleTopic;
        this.wallTopic = wallTopic;
        this.deployTopic = deployTopic;
        this.packTopic = packTopic;
    }

    @Bean
    public ReactiveKafkaConsumerTemplate<String, TowerEventDto> towerEventConsumerTemplate(KafkaProperties kafkaProperties) {
        ReceiverOptions<String, TowerEventDto> kafkaReceiverOptions = configureKafkaForTopic(kafkaProperties, towerTopic, TowerEventDto.class);
        return new ReactiveKafkaConsumerTemplate<>(kafkaReceiverOptions);
    }

    @Bean
    public ReactiveKafkaConsumerTemplate<String, CastleEventDto> castleEventConsumerTemplate(KafkaProperties kafkaProperties) {

        ReceiverOptions<String, CastleEventDto> kafkaReceiverOptions = configureKafkaForTopic(kafkaProperties, castleTopic, CastleEventDto.class);
        return new ReactiveKafkaConsumerTemplate<>(kafkaReceiverOptions);
    }

    @Bean
    public ReactiveKafkaConsumerTemplate<String, WallEventDto> wallEventConsumerTemplate(KafkaProperties kafkaProperties) {

        ReceiverOptions<String, WallEventDto> kafkaReceiverOptions = configureKafkaForTopic(kafkaProperties, wallTopic, WallEventDto.class);
        return new ReactiveKafkaConsumerTemplate<>(kafkaReceiverOptions);
    }

    @Bean
    public ReactiveKafkaConsumerTemplate<String, DeployEventDto> deployEventConsumerTemplate(KafkaProperties kafkaProperties) {

        ReceiverOptions<String, DeployEventDto> kafkaReceiverOptions = configureKafkaForTopic(kafkaProperties, deployTopic, DeployEventDto.class);
        return new ReactiveKafkaConsumerTemplate<>(kafkaReceiverOptions);
    }

    @Bean
    public ReactiveKafkaConsumerTemplate<String, PackEventDto> packEventConsumerTemplate(KafkaProperties kafkaProperties) {

        ReceiverOptions<String, PackEventDto> kafkaReceiverOptions = configureKafkaForTopic(kafkaProperties, packTopic, PackEventDto.class);
        return new ReactiveKafkaConsumerTemplate<>(kafkaReceiverOptions);
    }

    private <T> ReceiverOptions<String, T> configureKafkaForTopic(KafkaProperties kafkaProperties, String topic, Class<T> defaultType) {
        Map<String, Object> defaultConsumerProperties = kafkaProperties.buildConsumerProperties();

        defaultConsumerProperties.put(JsonDeserializer.VALUE_DEFAULT_TYPE, defaultType);

        return ReceiverOptions
                .<String, T>create(defaultConsumerProperties)
                .subscription(Collections.singletonList(topic));
    }

}
