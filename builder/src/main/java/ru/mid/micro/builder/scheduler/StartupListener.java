package ru.mid.micro.builder.scheduler;

import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.mid.micro.builder.service.kafka.consumer.KafkaConsumerService;

import java.util.Set;

@Component
public class StartupListener implements ApplicationListener<ApplicationStartedEvent> {

    private final Set<KafkaConsumerService> kafkaConsumerServices;

    public StartupListener(Set<KafkaConsumerService> kafkaConsumerServices) {
        this.kafkaConsumerServices = kafkaConsumerServices;
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        kafkaConsumerServices.forEach(KafkaConsumerService::run);
    }
}
