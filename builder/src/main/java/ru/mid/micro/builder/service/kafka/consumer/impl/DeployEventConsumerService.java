package ru.mid.micro.builder.service.kafka.consumer.impl;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import ru.mid.micro.builder.dto.kafka.event.DeployEventDto;
import ru.mid.micro.builder.dto.kafka.event.WallEventDto;
import ru.mid.micro.builder.service.kafka.consumer.KafkaConsumerService;

import java.time.Duration;

@Service
public class DeployEventConsumerService implements KafkaConsumerService {
    private static final Logger LOG = LoggerFactory.getLogger(DeployEventConsumerService.class);

    private final ReactiveKafkaConsumerTemplate<String, DeployEventDto> deployEventConsumerTemplate;

    public DeployEventConsumerService(ReactiveKafkaConsumerTemplate<String, DeployEventDto> deployEventConsumerTemplate) {
        this.deployEventConsumerTemplate = deployEventConsumerTemplate;
    }

    private Flux<DeployEventDto> consumeEvent() {
        return deployEventConsumerTemplate
                .receiveAutoAck()
                .delayElements(Duration.ofSeconds(2L)) // BACKPRESSURE
                .doOnNext(consumerRecord -> LOG.info("received key={}, value={} from topic={}, offset={}",
                        consumerRecord.key(),
                        consumerRecord.value(),
                        consumerRecord.topic(),
                        consumerRecord.offset())
                )
                .map(ConsumerRecord::value)
                .doOnNext(kafkaRequest -> LOG.info("successfully consumed {}={}", DeployEventDto.class.getSimpleName(), kafkaRequest))
                .doOnError(throwable -> LOG.error("something bad happened while consuming : {}", throwable.getMessage()))
                .onErrorContinue((e,f)  -> {
                    LOG.error("shiii... {}", f, e);
                });
    }

    @Override
    public void run() {
        consumeEvent().subscribe();
    }
}
